package main

import (
	"context"
	"fmt"
	"log-group-importer/pkg/aws"
	"strings"
)

const RetentionInDays = 7

var Template = `resource "aws_cloudwatch_log_group" "%s" {
	name              = "%s"
	retention_in_days = %d
	provider		   = aws.ca 
}
`

var ImportTemplate = `# ./pipeline/import-resource-layer.sh pr 400-logs aws_cloudwatch_log_group.%s %s`

func main() {
	ctx := context.Background()
	awsClient := aws.NewAWSClient(ctx)
	names := awsClient.FindLogs(ctx)

	for _, name := range names {
		resourceName := findResourceName(name, "/")
		entry := fmt.Sprintf(Template, resourceName, name, RetentionInDays)
		cmdEntry := fmt.Sprintf(ImportTemplate, resourceName, name)

		fmt.Println(cmdEntry)
		fmt.Println(entry)
		fmt.Println()
		fmt.Println()
	}
}

func findResourceName(in string, substr string) string {
	index := strings.LastIndex(in, substr)
	return in[index+1:]
}

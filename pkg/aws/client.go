package aws

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatchlogs"
)

type AWSClient struct {
	client cloudwatchlogs.Client
}

func NewAWSClient(ctx context.Context) *AWSClient {
	awsClient := AWSClient{}
	awsClient.setAWSClient("ca-central-1", ctx)
	return &awsClient
}

func (a *AWSClient) setAWSClient(region string, ctx context.Context) {
	cfg, err := config.LoadDefaultConfig(ctx, config.WithRegion(region))

	if err != nil {
		log.Fatal("Error setting config:", err)
	}

	a.client = *cloudwatchlogs.NewFromConfig(cfg)
}

func (a *AWSClient) FindLogs(ctx context.Context) []string {
	input := cloudwatchlogs.DescribeLogGroupsInput{}
	results, err := a.client.DescribeLogGroups(ctx, &input)
	if err != nil {
		log.Fatal("Error fetching log groups:", err)
	}

	names := []string{}
	for _, result := range results.LogGroups {
		names = append(names, *result.LogGroupName)
	}
	return names
}
